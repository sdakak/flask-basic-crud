import os
from flask import abort
from flask import jsonify
from flask import make_response
from flask import request
from flask import url_for

from data_provider_service import DataProviderService

db_engine = os.environ['JAWSDB_URL']

DATA_PROVIDER = DataProviderService(db_engine)


def add_change():
  status = request.form["status"]
  description = request.form["description"]
  user = request.form["user"]
  sn_name = request.form["sn_name"]
  sn_child_of = request.form["sn_child_of"]

  new_change_id = DATA_PROVIDER.add_change(status=status,
                                           description=description,
                                           user=user,
                                           sn_name=sn_name,
                                           sn_child_of=sn_child_of)

  if not new_change_id:
    return abort(400)

  return jsonify({
    "id": new_change_id,
    "url": url_for("change_by_id", id=new_change_id)
  })


def change(serialize=True):
  changes = DATA_PROVIDER.get_change(serialize=serialize)
  if serialize:
    return jsonify({"changes": changes, "total": len(changes)})
  else:
    return changes


def change_by_id(id):
  change = DATA_PROVIDER.get_change(id, serialize=True)
  if change:
    return jsonify({"change": change})
  else:
    #
    # In case we did not find the schema_node by id
    # we send HTTP 404 - Not Found error to the client
    #
    abort(404)


def update_change(id):
  new_change = {
    "status": request.form["status"],
    "description": request.form["description"],
    "user": request.form["user"],
    "sn_name": request.form["sn_name"],
    "sn_child_of": request.form["sn_child_of"]
  }
  updated_change = DATA_PROVIDER.update_change(id, new_change)
  if not updated_change:
    abort(404)
  else:
    return jsonify({"change": updated_change})


def delete_change(id):
  if DATA_PROVIDER.delete_change(id):
    return make_response('', 200)
  else:
    return abort(404)


def initialize_database():
  DATA_PROVIDER.init_database()


def fill_database():
  DATA_PROVIDER.fill_database()


def build_message(key, message):
  return {key: message}
