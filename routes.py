from flask import abort
from flask import current_app
from flask import flash
from flask import jsonify
from flask import render_template

from middleware import add_change
from middleware import build_message
from middleware import change
from middleware import change_by_id
from middleware import delete_change
from middleware import fill_database as fill_db
from middleware import initialize_database as init_db
from middleware import update_change


def init_api_routes(app):
  if app:
    app.add_url_rule('/api/change', 'add_change', add_change, methods=['POST'])
    app.add_url_rule('/api/change/<string:id>', 'change_by_id', change_by_id,
                     methods=['GET'])
    app.add_url_rule('/api/change', 'change', change, methods=['GET'])
    app.add_url_rule('/api/change/<string:id>', 'update_change', update_change,
                     methods=['PUT'])
    app.add_url_rule('/api/change/delete/<string:id>', 'delete_change',
                     delete_change, methods=['DELETE'])

    app.add_url_rule('/api/initdb', 'initdb', initialize_database)
    app.add_url_rule('/api/filldb', 'filldb', fill_database)
    app.add_url_rule('/api', 'list_routes', list_routes, methods=['GET'],
                     defaults={'app': app})


def page_about():
  if current_app:
    flash('The application was loaded', 'info')

  return render_template('about.html', selected_menu_item="about")


def page_change():
  current_change = change(serialize=False)
  return render_template('change.html', selected_menu_item="change",
                         changes=current_change)


def page_add_change():
  return render_template('add_change.html')


def page_index():
  return render_template('index.html', selected_menu_item="index")


def crash_server():
  abort(500)


def initialize_database():
  message_key = "Initialize Database"
  try:
    init_db()
  except ValueError as err:
    return jsonify(build_message(message_key, err.message))

  return jsonify(build_message(message_key, "OK"))


def fill_database():
  message_key = "Fill Database"
  try:
    fill_db()
  except ValueError as err:
    return jsonify(build_message(message_key, err.message))

  return jsonify(build_message(message_key, "OK"))


def init_website_routes(app):
  if app:
    app.add_url_rule('/crash', 'crash_server', crash_server, methods=['GET'])
    app.add_url_rule('/about', 'page_about', page_about, methods=['GET'])
    app.add_url_rule('/change', 'page_change', page_change, methods=['GET'])
    app.add_url_rule('/change/add', 'page_add_change', page_add_change,
                     methods=['GET'])
    app.add_url_rule('/', 'page_index', page_index, methods=['GET'])


def handle_error_404(error):
  flash('Server says: {0}'.format(error), 'error')
  return render_template('404.html', selected_menu_item=None)


def handle_error_500(error):
  flash('Server says: {0}'.format(error), 'error')
  return render_template('500.html', selected_menu_item=None)


def init_error_handlers(app):
  if app:
    app.error_handler_spec[None][404] = handle_error_404
    app.error_handler_spec[None][500] = handle_error_500


def list_routes(app):
  result = []
  for rt in app.url_map.iter_rules():
    result.append({
      'methods': list(rt.methods),
      'route': str(rt)
    })
  return jsonify({'routes': result, 'total': len(result)})
