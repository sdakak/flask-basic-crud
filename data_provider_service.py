from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from Models import Change
from Models import init_database


class DataProviderService:
  def __init__(self, engine):
    """
    :param engine: The engine route and login details
    :return: a new instance of DAL class
    :type engine: string
    """
    if not engine:
      raise ValueError(
        'The values specified in engine parameter has to be supported by SQLAlchemy')
    self.engine = engine
    db_engine = create_engine(engine)
    db_session = sessionmaker(bind=db_engine)
    self.session = db_session()

  def init_database(self):
    pass
    """
    Initializes the database tables and relationships
    :return: None
    """
    init_database(self.engine)

  def add_change(self, status, description, user, sn_name, sn_child_of):
    """
    Creates and saves a new schema change request to the database.
    """

    new_change = Change(status=status,
                        description=description,
                        user=user,
                        sn_name=sn_name,
                        sn_child_of=sn_child_of)

    try:
      self.session.add(new_change)
      self.session.commit()
    except:
      self.session.rollback()
      return False

    return new_change.id

  def get_change(self, id=None, serialize=False):
    """
    If the id parameter is  defined then it looks up the change with the given id,
    otherwise it loads all the proposed changes
    """

    all_changes = []

    if id is None:
      all_changes = self.session.query(Change).order_by(Change.id).all()
    else:
      all_changes = self.session.query(Change).filter(Change.id == id).all()

    if serialize:
      return [chg.serialize() for chg in all_changes]
    else:
      return all_changes

  def update_change(self, id, new_change):
    updated_change = None
    changes = self.get_change(id)
    if not changes:
      return False

    change = changes[0]

    if change:
      change.status = new_change["status"]
      change.description = new_change["description"]
      change.user = new_change["user"]
      change.sn_name = new_change["sn_name"]
      change.sn_child_of = new_change["sn_child_of"]
      try:
        self.session.add(change)
        self.session.commit()
      except:
        self.session.rollback()
        return False

      updated_change = self.get_change(id)[0]

    return updated_change.serialize()

  def delete_change(self, id):
    if id:
      candidate_changes = self.get_change(id)
      if not candidate_changes:
        return False

      candidate_change = self.get_change(id)[0]

      try:
        self.session.delete(candidate_change)
        self.session.commit()
      except:
        self.session.rollback()
        return False

      if candidate_change:
        return True
    return False

  def fill_database(self):
    #
    # Changes Proposed
    #
    cn1 = Change(status="Pending Approval",
                 description="new fax product",
                 user="jdoe",
                 sn_name="fax",
                 sn_child_of=1,
                 )

    cn2 = Change(status="Pending Approval",
                 description="outgoing calls to hawaii",
                 user="jdoe",
                 sn_name="outgoing_calls_us_hawaii",
                 sn_child_of=12,
                 )

    cn3 = Change(status="Pending Approval",
                 description="incoming collect calls to us",
                 user="jdoe",
                 sn_name="incoming_calls_us_collect",
                 sn_child_of=8,
                 )

    self.session.add(cn1)
    self.session.add(cn2)
    self.session.add(cn3)
    self.session.commit()
