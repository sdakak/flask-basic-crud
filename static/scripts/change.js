function saveChange() {
    var status = $("#inputStatus").val();
    var description = $("#inputDesc").val();
    var user = $("#inputUser").val();
    var sn_name = $("#inputSNName").val();
    var sn_child_of = $("#inputSNChildOf").val();

    var success_alert = $("#success_alert");

    $.post("/api/change", {
                status : status,
                description: description,
                user: user,
                sn_name: sn_name,
                sn_child_of: sn_child_of}, function(response){
                    $(success_alert).find("#success_message").text("Schema Change Proposal successfully created with ID:" + response.id);
                    $(success_alert).removeClass("hidden");
    });
}

